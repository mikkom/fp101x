import Control.Monad (liftM, ap)

main = putStrLn ""

type State = Int

data ST a = S (State -> (a, State))

apply (S f) x = f x

instance Functor ST where
  fmap = liftM

instance Applicative ST where
  pure = return
  (<*>) = ap

instance Monad ST where
  return x = S (\s -> (x, s))
  st >>= f = S (\s -> let (x, s') = apply st s in apply (f x) s')


data Tree a = Leaf a | Node (Tree a) (Tree a)

fresh = S (\n -> (n, n + 1))

mlabel (Leaf x) = do n <- fresh
                     return (Leaf (x, n))
mlabel (Node l r) = do l' <- mlabel l
                       r' <- mlabel r
                       return (Node l' r')

label t = fst (apply (mlabel t) 0)

--let tree = Node (Node (Leaf 'a') (Leaf 'b')) (Leaf 'c') in
--  label tree

printTree (Leaf x) = show x
printTree (Node l r) = "Node  (" ++ printTree l ++ ") (" ++ printTree r ++ ")"
