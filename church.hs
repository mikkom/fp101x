
type Church (a -> a) -> a -> a

zero = \s z -> z
     = id

one = \s z -> s z
    = \s -> s

two = \s z -> s (s z)
    = \s z -> (s . s) z
    = \s -> s . s

c2i x = x (+1) 0

c2i two = 2

c2s x = x ('*':) ""

c2s two = "**"


add x y = \s z -> x s (y s z)

mul x y = \s z -> x (y s) z
        = \s z -> x . y
        =  x . y
