module Hw3 where

all1 p xs = and (map p xs)

all3 p = and . map p

all4 p = not . any (not . p)

all6 p xs = foldl (&&) True (map p xs)

all7 p xs = foldr (&&) False (map p xs)

all8 p = foldr (&&) True . map p

any2 p = or . map p

any3 p xs = length (filter p xs) > 0

any4 p = not . null. dropWhile (not . p)

any5 p = null . filter p

any6 p xs = not (all (\x -> not (p x)) xs)

any7 p xs = foldr (\x acc -> (p x) || acc) False xs

any8 p xs = foldr (||) True (map p xs)

dropWhile4 p = foldl add []
  where add [] x = if p x then [] else [x]
        add acc x = x : acc

dec2int xs = foldl (\ x y -> 10 * x + y) 0 xs

compose fs = foldr (.) id fs

-- sumsqreven = compose [sum, map (^ 2), filter even]

curry3 f = \x y -> f (x, y)

uncurry1 f = \(x, y) -> f x y

chop8 [] = []
chop8 bits = take 8 bits : chop8 (drop 8 bits)

unfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)

mychop8 = unfold null (take 8) (drop 8)

mymap f = unfold null (f . head) tail

myiterate f = unfold (const False) id f
