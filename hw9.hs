module Main where
import Data.List
import Data.Char
-- import Hugs.IOExts (unsafeCoerce)
-- import GHC.Base
import Unsafe.Coerce

data Nat = Zero
         | Succ Nat
         deriving Show

natToInteger Zero = 0
natToInteger (Succ n) = natToInteger n + 1

natToInteger6 = head . m
  where m Zero = [0]
        m (Succ n) = [sum [x | x <- (1 : m n)]]

integerToNat 0 = Zero
integerToNat n = Succ (integerToNat (n - 1))

add Zero n = n
add (Succ m) n = Succ (add m n)

mult m Zero = Zero
mult m (Succ n) = add m (mult m n)

data Tree = Leaf Integer
          | Node Tree Integer Tree

occurs m (Leaf n) = m == n
occurs m (Node l n r) =
  case compare m n of
  LT -> occurs m l
  EQ -> True
  GT -> occurs m r

occurs2 m (Leaf n) = m == n
occurs2 m (Node l n r)
  | m == n = True
  | m < n = occurs m l
  | otherwise = occurs m r

data LTree = Leaf Integer
           | Node Tree Tree
