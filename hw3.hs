
safetail1 xs = if null xs then [] else tail xs

safetail2 [] = []
safetail2 (_ : xs) = xs

safetail4 xs
  | null xs = []
  | otherwise = tail xs

safetail6 [] = []
safetail6 xs = tail xs


safetail7 [x] = [x]
safetail7 (_ : xs) = xs

safetail8
  = \ xs ->
      case xs of
          [] -> []
          (_ : xs) -> xs


b ||| c
  | b == c = b
  | otherwise = True

a &&& b = if b then a else False

mult = \ x -> (\ y -> (\ z -> x * y * z))

mult2 = mult 2

mult6 = mult 3 2

funct :: Int -> [a] -> [a]
funct x xs = take (x + 1) xs ++ drop x xs

e3 x = x * 2

e4 (x, y) = x

e6 x y = x * y

e9 [x, y] = (x, True)

e10 (x, y) = [x, y]

main = putStrLn "Hello World"
