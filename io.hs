module MyIO where

putStr' [] = return ()
putStr' (x : xs) = putChar x >> putStr' xs

putStrLn' [] = putChar '\n'
putStrLn' xs = putStr' xs >> putStr' "\n"

getLine' = get ""

get xs = do x <- getChar
            case x of
              '\n' -> return xs
              _ -> get (xs ++ [x])

interact' f
  = do input <- getLine'
       putStrLn' (f input)

sequence_' :: Monad m => [m a] -> m ()
sequence_' [] = return ()
sequence_' (m : ms) = m >>= \_ -> sequence_' ms

sequence' :: Monad m => [m a] -> m [a]
sequence' [] = return []
sequence' (m : ms)
  = do a <- m
       as <- sequence' ms
       return (a : as)

mapM1' f as = sequence' (map f as)

mapM2' f [] = return []
mapM2' f (a : as)
  = f a >>= \b -> mapM2' f as >>= \bs -> return (b : bs)

mapM3' f as = sequence_' (map f as)

--mapM4' f [] = return []
--mapM4' f (a : as)
--  = f a >> \b -> mapM4' f as >> \bs -> return (b : bs)

mapM6' f [] = return []
mapM6' f (a : as)
  = do b <- f a
       bs <- mapM6' f as
       return (b : bs)

mapM7' f [] = return []
mapM7' f (a : as)
  = f a >>=
      \b ->
        do bs <- mapM7' f as
           return (b : bs)

mapM8' f [] = return []
mapM8' f (a : as)
  = f a >>=
      \b ->
        do bs <- mapM8' f as
           return (bs ++ [b])

filterM' _ [] = return []
filterM' p (x : xs)
  = do flag <- p x
       ys <- filterM' p xs
       if flag then return (x : ys) else return ys

foldLeftM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldLeftM f a [] = return a
foldLeftM f a (x : xs) =
  do acc <- f a x
     foldLeftM f acc xs

foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
foldRightM f b [] = return b
foldRightM f b (x : xs) =
  do acc <- foldRightM f b xs
     f x acc

liftM1 f m
  = do x <- m
       return (f x)

liftM2 f m = m >>= \a -> f a

liftM3 f m = m >>= \a -> return (f a)

liftM5 f m = m >>= \a -> m >>= \b -> return (f a)

liftM6 f m = m >>= \a -> m >>= \b -> return (f b)

liftM7 f m = mapM f [m]

liftM8 f m = m >> \a -> return (f a)
