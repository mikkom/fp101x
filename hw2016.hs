
foldl' f a bs = foldr (\b -> \g -> (\a -> g (f a b))) id bs a

foldl'' f a bs = foldr (\a b -> f b a) a bs

foldl''' f = flip $ foldr (\a b g -> b (f g a)) id
