
double x = x + x

quadruple x = double $ double x

factorial n = product [1..n]

average ns = sum ns `div` length ns

n = a `div` length xs
  where a = 10
        xs = [1..5]

increment n = n + 1

-- qsort [] = []
-- qsort (x : xs) = qsort smaller ++ [x] ++ qsort larger
--   where smaller = [a | a <- xs, a <= x]
--         larger  = [b | b <- xs, b > x]

-- rsort [] = []
-- rsort (x : xs) = rsort larger ++ [x] ++ rsort smaller
--   where smaller = [a | a <- xs, a <= x]
--         larger  = [b | b <- xs, b > x]

rsort [] = []
rsort xs = x : rsort larger ++ rsort smaller
  where x = maximum xs
        smaller = [a | a <- xs, a < x]
        larger  = [b | b <- xs, b >= x]


qsort [] = []
qsort (x : xs) = reverse (qsort smaller ++ [x] ++ qsort larger)
  where smaller = [a | a <- xs, a <= x]
        larger  = [b | b <- xs, b > x]

last1 xs = drop (length xs - 1) xs

last2 xs = head (drop (length xs - 1) xs)

last3 xs = xs !! (length xs - 1)

init1 xs = take (length xs - 1) xs

qsort1 [] = []
qsort1 (x : xs) = reverse (reverse (qsort1 smaller) ++ [x] ++ reverse (qsort1 larger))
  where smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

palindrome xs = reverse xs == xs

twice f x = f (f x)
