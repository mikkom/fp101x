
halve xs = (take n xs, drop n xs)
           where n = length xs `div` 2

safetail1 xs = if null xs then [] else tail xs

safetail2 [] = []
safetail2 (_ : xs) = xs

safetail3 xs
  | null xs = []
  | otherwise = tail xs

safetail4 [] = []
safetail4 xs = tail xs

safetail5 [x] = [x]
safetail5 (_ : xs) = xs

safetail6
  = \ xs ->
     case xs of
     [] -> []
     (_ : xs) -> xs

remove2 n xs = take n xs ++ drop (n + 1) xs

funct :: Int -> [a] -> [a]
funct x xs = take (x + 1) xs ++ drop x xs

e3 x = x * 2
e4 (x, y) = x
e6 x y = x * y
