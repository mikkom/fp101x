
pyths n = [(x, y, z) | x <- [1..n], y <- [1..n], z <- [1..n], x^2 + y^2 == z^2]

factors n = [x | x <- [1..n], mod n x == 0]

perfects n = [x | x <- [1..n], isPerfect x]
  where isPerfect num = sum (init (factors num)) == num

find :: (Eq a) => a -> [(a,b)] -> [b]
find k t = [v | (k', v) <- t, k == k']

positions x xs = find x (zip xs [0..n])
  where n = length xs -1

riffle xs ys = concat [[x, y] | (x, y) <- zip xs ys]

divides n x = mod n x == 0

divisors x = [d | d <- [1..x], divides x d]
