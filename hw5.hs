
and2 [] = True
and2 (b : bs)
  | b = and2 bs
  | otherwise = False

concat2 [] = []
concat2 (xs : xss) = xs ++ concat xss

(x : _) !!! 0 = x
(_ : xs) !!! n = xs !!! (n - 1)

elem2 _ [] = False
elem2 x (y : ys)
  | x == y = True
  | otherwise = elem2 x ys

merge [] ys = ys
merge xs [] = xs
merge (x : xs) (y : ys)
  = if x <= y then x : merge xs (y : ys) else y : merge (x : xs) ys

halve xs = splitAt (div (length xs) 2) xs

msort [] = []
msort [x] = [x]
msort xs = merge (msort ys) (msort zs)
  where (ys, zs) = halve xs
